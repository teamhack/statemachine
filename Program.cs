﻿using System;
using System.Threading.Tasks;

namespace StateMachineTesting
{
    class Program
    {
        static void Main(string[] args)
        {

            MainAsync(args).GetAwaiter().GetResult();
            Console.ReadLine();
        }

        static async Task MainAsync(string[] args)
        {

            //If you make a mistake and enter a state that's not allowed it will throw an exception.
            //The exception can be intercepted by the state machine to perform a specific action.

            Document document = new Document();
            await document.BeginProofreadingAsync();
            await document.BeginBrandingReviewChangesAsync();
            await document.BeginFinalApprovalAsync();
            await document.MakeDocumentReviewChangeAsync("Change the logo it's awful.");  //Just demo you can pass parameters
            await document.BrandingChangesCompleteAsync();


            Document document2 = new Document();
            await document2.BeginProofreadingAsync();
            await document2.BeginBrandingReviewChangesAsync();
            await document2.BeginFinalApprovalAsync();
            await document2.MakeDocumentReviewChangeAsync("Change the logo it's awful."); //Just demo you can pass parameters
            await document2.BrandingChangesApproveAsync();
            await document2.BeginFinalApprovalAsync();

        }
    }
}
