﻿using Stateless;
using Stateless.Graph;
using System;
using System.Threading.Tasks;

namespace StateMachineTesting
{
    public class Document
    {

        //State is like "Stages"
        private enum State
        {
            Draft,
            Proofreading,
            BrandingChangesRequested,
            Branding,
            FinalApproval,
            Complete,
            Rejected,
            Approve
        }


        //Triggers are like "Task Responses", sort of....
        private enum Triggers
        {
            //UpdateDocument,
            //BeginReview,
            ChangedNeeded,
            //Accept,
            //Reject,
            //Submit,
            //Decline,
            //RestartReview,
            Approve,
            Reject,
            Complete
        }

        private readonly StateMachine<State, Triggers> machine;
        private readonly StateMachine<State, Triggers>.TriggerWithParameters<string> changedNeededParameters;

        public Document()
        {

            //Load current state from database
            //machine = new StateMachine<State, Triggers>(() => state, s => state = s);



            machine = new StateMachine<State, Triggers>(State.Draft);  //Starting state is draft.

            machine.Configure(State.Draft)
                .PermitReentry(Triggers.Reject)
                .Permit(Triggers.Approve, State.Proofreading)
                .OnEntryAsync(OnDraftEntryAsync)  //You can hook in here to setup tasks, notifications, etc.
                .OnExitAsync(OnDraftExitAsync); //You can hook in here to ... maybe let the author know it's left draft.

            changedNeededParameters = machine.SetTriggerParameters<string>(Triggers.ChangedNeeded);  //Ability to pass parameters - string in this case.

            machine.Configure(State.Proofreading)
                .Permit(Triggers.Approve, State.Branding)
                .OnEntryAsync(OnProofreadingEntryAsync)  
                .OnExitAsync(OnProofreadingExitAsync);

            machine.Configure(State.Branding)
              .Permit(Triggers.Approve, State.FinalApproval)
              .OnEntryAsync(OnBrandingEntryAsync)
              .OnExitAsync(OnBrandingExitAsync);

            machine.Configure(State.FinalApproval)
                .Permit(Triggers.Approve, State.Complete)
                .Permit(Triggers.ChangedNeeded, State.BrandingChangesRequested)
                .Permit(Triggers.Reject, State.Rejected)
                .OnEntryAsync(OnFinalApprovalEnterAsync)
                .OnExitAsync(OnFinalApprovalExitAsync);

            machine.Configure(State.BrandingChangesRequested)
                .Permit(Triggers.Approve, State.FinalApproval)
                .Permit(Triggers.Complete, State.Complete)
                .OnEntryAsync(OnBrandingChangesRequestedEntryAsync)
                .OnExitAsync(OnBrandingChangesRequestedExitAsync);

            machine.Configure(State.Rejected)
                //.Permit(Triggers.RestartReview, State.Review)
                .OnEntryAsync(OnRejectedEnterAsync)
                .OnExitAsync(OnRejectedExitAsync);

            machine.Configure(State.Complete)
                .OnEntryAsync(OnCompleteEnter);


            //This generates an UML diagram of the workflow.
            //https://dreampuf.github.io/GraphvizOnline
            //Use something like the above website to view it.
            string result = UmlDotGraph.Format(machine.GetInfo());
            Console.WriteLine();
        }

        //Events
        private async Task OnBrandingChangesRequestedEntryAsync()
        {
            Console.WriteLine("OnBrandingChangesRequestedEntryAsync");
        }
        private async Task OnBrandingChangesRequestedExitAsync()
        {
            Console.WriteLine("OnBrandingChangesRequestedExitAsync");
        }

        private async Task OnFinalApprovalEnterAsync()
        {
            Console.WriteLine("OnFinalApprovalEnterAsync");
        }

        private async Task OnFinalApprovalExitAsync()
        {
            Console.WriteLine("OnFinalApprovalExitAsync");
        }

        private async Task OnRejectedEnterAsync()
        {
            Console.WriteLine("OnRejectedEnterAsync");
        }

        private async Task OnRejectedExitAsync()
        {
            Console.WriteLine("OnRejectedExitAsync");
        }

        private async Task OnCompleteEnter()
        {
            Console.WriteLine("OnCompleteEnter");
        }

        private async Task OnDraftEntryAsync()
        {
            Console.WriteLine("OnDraftEntry");
        }

        private async Task OnDraftExitAsync()
        {
            Console.WriteLine("OnDraftExit");
        }

        private async Task OnProofreadingEntryAsync()
        {
            //Assign tasks, send notifications... whatever we need to do for this "state".
            Console.WriteLine("OnProofreadingEntry");
        }

        private async Task OnProofreadingExitAsync()
        {
            //Maybe we want to send a notification to the author that it has been reviewed.
            Console.WriteLine("OnProofreadingExit");
        }

        private async Task OnBrandingEntryAsync()
        {
            Console.WriteLine("OnBrandingEntry");
        }

        private async Task OnBrandingExitAsync()
        {
            Console.WriteLine("OnBrandingExit");
        }


        //Public interfaces to control states.  States are controlled by the configuration of the current state.
        public async Task UpdateDocumentAsync() => await machine.FireAsync(Triggers.Approve);

        public async Task BeginProofreadingAsync() => await machine.FireAsync(Triggers.Approve);

        public async Task MakeDocumentReviewChangeAsync(string change) => await machine.FireAsync(changedNeededParameters, change);

        public async Task BeginBrandingReviewChangesAsync() => await machine.FireAsync(Triggers.Approve);

        public async Task BeginFinalApprovalAsync() => await machine.FireAsync(Triggers.Approve);

        public async Task BrandingChangesCompleteAsync() => await machine.FireAsync(Triggers.Complete);

        public async Task BrandingChangesApproveAsync() => await machine.FireAsync(Triggers.Approve);

    }

}
